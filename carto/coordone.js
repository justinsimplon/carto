
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//I create a reset function to reload the script to clear the path//
function Reset() {
  location.reload();
}
//we recover the various data via the url//
const url = "https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=t_troncon_cycle&facet=typeamenag&rows=306";
const urlbis = "https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=t_troncon_line&facet=nom_ligne&rows=24"


//we create a bike function to retrieve the data containing the path/
function Velo(){
    fetch(url)
    //Code for handling the data from the API
    .then(resp => {
        //In this case return a JSON
        return resp.json();
    })
    .then(response => {
        //Loop on each records
        for (let i = 0; i < response.records.length; i++) {
            //localisation marker with a pop up when the user clicks on the marker
            var coordinates = response.records[i].fields.geo_shape.coordinates;
            var coordinates_revers = coordinates.map(function(cord){
              return cord.reverse();

            })
            //we show the route
            L.polyline (coordinates_revers, {color: 'red'})
            .addTo(mymap);
    };
    })
}


function reverseCoordinateInList(coordinateList) {
  var cordarray = [];
  
  coordinateList.map(function (cord) {
    // console.log(cord);
    return cord.reverse();
  });
  return coordinateList;
  console.log(coordinateList);
  for (let j = 0; j < coordinateList.length; j++) {
    // console.log(j);
    var coordinateListreversed = coordinateList[j].map(function (cord) {
      // console.log(cord);
      return cord.reverse();
    });
    // console.log(coordinateListreversed);
    // console.log(coordinateListreversed);
    cordarray.push(coordinateListreversed)

  }

  // il rentre dans la list
  // il rentre dans la list de la list
  // il repcupere se qui corespon aux coordoné dans la list
  // il inverse les coordoné dans la list 
  // puis retourne les list avec les coordoné inverser
  return cordarray;
}

// function analyze(list)
// {
//   let good = 0;

//   for(let i=0; i < list.length; i++) {
//     for(let j=0; j < list[i].length; j++) {
//         if(list[i][j][0] > list[i][j][1]) {
//           if(!good) {
//             console.log("good:" + i +","+ j);
//             good = 1;
//           }
//         } else {
//           if(good) {
//             console.log("not good:" + i +","+ j);
//             good = 0;
//           }
//         }
//     }
//   }
// }
//we create a bus function to retrieve the data of the trip
function Bus() {
  fetch(urlbis)
    //Code for handling the data from the API
    .then(resp => {
      //In this case return a JSON
      return resp.json()
    })
    .then(response => {
      //Loop on each records
      for (let i = 0; i < response.records.length; i++) {
        var multiCoordinateList = response.records[i].fields.geo_shape.coordinates
       var res = [];
       
        for(let j = 0; j < multiCoordinateList.length; j++) {
          //we show the route
          L.polyline(reverseCoordinateInList(multiCoordinateList[j]))
          .addTo(mymap);
        
        }
      
      }
    

    });

}